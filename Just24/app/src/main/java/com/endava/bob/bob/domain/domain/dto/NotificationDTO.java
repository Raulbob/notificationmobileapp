package com.endava.bob.bob.domain.domain.dto;

import com.endava.bob.bob.domain.constants.NotificationStatus;
import com.endava.bob.bob.domain.constants.NotificationTypeEnum;

import java.util.Date;

public class NotificationDTO {

    private String id;

    private String creationDate;
    private Date viewDate;
    private NotificationTypeEnum type;
    private NotificationStatus status;
    private String message;
    private String userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NotificationTypeEnum getType() {
        return type;
    }

    public void setType(NotificationTypeEnum type) {
        this.type = type;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    /**
     * If status is "VIEWED", the viewDate will be updated.
     */
    public void setStatus(NotificationStatus status) {
        if (status.equals(NotificationStatus.VIEWED)) {
            setViewDate(new Date());
        }
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getViewDate() {
        return viewDate;
    }

    public void setViewDate(Date viewDate) {
        this.viewDate = viewDate;
    }

}
