package com.endava.bob.bob.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.domain.domain.dto.TaskDTO;
import com.endava.bob.bob.service.callback.MyTaskCallback;
import com.endava.bob.bob.service.restClient.Manager;
import com.endava.bob.bob.service.util.Converter;
import com.endava.bob.bob.view.adapter.TaskAdapter;
import com.endava.bob.bob.view.decorator.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TaskFragment extends Fragment implements MyTaskCallback {

    private OnListFragmentInteractionListener mListener;
    private List<Just24Task> tasks = new ArrayList<>();
    private TaskAdapter adapter;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private Snackbar taskLoadError;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TaskFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tasks_fragment, container, false);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress);

        // Set the adapter
        if (view instanceof RecyclerView) {
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new TaskAdapter(tasks, mListener);
            recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity().getApplicationContext(),
                    R.drawable.item_decorator)));

            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity myActivity = (AppCompatActivity) getActivity();
        myActivity.getSupportActionBar().setTitle("Tasks");
        //Refresh button
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.refresh_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Manager.loadTasks(getActivity(), TaskFragment.this, progressBar);
            }
        });
        Manager.loadTasks(getActivity(), this, progressBar);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (taskLoadError != null)
            taskLoadError.dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void add(TaskDTO task) {
        Converter c = new Converter();
        adapter.addTask(c.convertWithHour(task));
    }

    @Override
    public void clear() {
        adapter.clear();
    }

    @Override
    public void showError(String error) {
        Log.d("RAUL BOB:", "Show error in justTaskFragment");
        progressBar.setVisibility(View.GONE);
        // snackbarPosition
        // CoordinatorLayout cl = (CoordinatorLayout) getActivity().findViewById(R.id.snackbarPosition);
        if (recyclerView != null && !isDetached() && isVisible()) {
            taskLoadError.setText(error);
            taskLoadError.show();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        taskLoadError = Snackbar.make(recyclerView, "", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Manager.loadTasks(getActivity(), TaskFragment.this, progressBar);
                    }
                });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Just24Task item);
    }
}
