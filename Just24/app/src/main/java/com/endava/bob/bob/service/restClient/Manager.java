package com.endava.bob.bob.service.restClient;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.endava.bob.bob.domain.Notification;
import com.endava.bob.bob.domain.TaskComment;
import com.endava.bob.bob.domain.domain.dto.CommentDTO;
import com.endava.bob.bob.domain.domain.dto.NotificationDTO;
import com.endava.bob.bob.domain.domain.dto.TaskDTO;
import com.endava.bob.bob.domain.domain.dto.UserDTO;
import com.endava.bob.bob.service.callback.MyAuthCallback;
import com.endava.bob.bob.service.callback.MyNotifCallback;
import com.endava.bob.bob.service.callback.MyTaskCallback;
import com.endava.bob.bob.view.fragment.CommentsFragment;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Manager class that handles interaction with Just24 Web Service using {@link EntityService}.
 * If a connection is opened for getTasks HttpRequest or getNotification HttpRequest no new request will be made to the server for the specific call.
 * <p>
 * Created by Raul on 3/13/2017.
 */
public class Manager {

    public static volatile boolean GET_TASKS_CONNECTION_OPEN = false;
    public static volatile boolean GET_NOTIFICATIONS_CONNECTION_OPEN = false;

    private static EntityService service = ServiceFactory.createRetrofitService(EntityService.class, EntityService.SERVICE_ENDPOINT);

    public boolean networkConnectivity(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public synchronized static void authenticate(final MyAuthCallback callback, String username, String password) {
        String base = username + ":" + password;
        String authHeader = "Basic " + Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);

        service.authenticate(authHeader, new UserDTO(false, null, username, password, null))
                .timeout(8, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserDTO>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.d("Raul:",e.getMessage());
                        if (e.getMessage().contains("Unauthorized") || e.getMessage().contains("HTTP 401")) {
                            callback.authFailed();
                            return;

                        }
                        callback.showError("Error while connecting to the server..");
                    }

                    @Override
                    public void onNext(UserDTO userDTO) {
                        if (userDTO.isAuthenticated()) {
                            callback.authSucceeded(userDTO);
                        } else {
                            callback.authFailed();
                        }
                    }
                });
    }


    public synchronized static void loadTasks(Activity activity, final MyTaskCallback callback, final ProgressBar progressBar) {
        String authHeader = getAuthHeaderFromSharedPreferences(activity);

        if (GET_TASKS_CONNECTION_OPEN) {
            return;
        } else {
            GET_TASKS_CONNECTION_OPEN = true;
            progressBar.setVisibility(View.VISIBLE);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
            String username = prefs.getString("USERNAME", null);
            service.getTasks(authHeader, username)
                    .timeout(8, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<TaskDTO>>() {

                        @Override
                        public void onError(Throwable e) {
                            GET_TASKS_CONNECTION_OPEN = false;
                            e.printStackTrace();
                            callback.clear();
                            callback.showError("Error when loading the Tasks. Please retry.");
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onNext(final List<TaskDTO> tasks) {
                            callback.clear();
                            for (TaskDTO task : tasks) {
                                Log.i("RB:", task.toString());
                                callback.add(task);
                            }
                        }

                        @Override
                        public void onCompleted() {
                            GET_TASKS_CONNECTION_OPEN = false;
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }


    public synchronized static void loadNotifications(Activity activity, final MyNotifCallback callback, final ProgressBar progressBar) {
        String authHeader = getAuthHeaderFromSharedPreferences(activity);

        if (GET_NOTIFICATIONS_CONNECTION_OPEN) {
            return;
        } else {
            GET_NOTIFICATIONS_CONNECTION_OPEN = true;
            progressBar.setVisibility(View.VISIBLE);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
            String username = prefs.getString("USERNAME", null);
            service.getNotifications(authHeader, username)
                    .timeout(8, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<NotificationDTO>>() {

                        @Override
                        public void onError(Throwable e) {
                            GET_NOTIFICATIONS_CONNECTION_OPEN = false;
                            e.printStackTrace();
                            callback.clear();
                            callback.showError("Error when loading the Notifications. Please retry.");
                        }

                        @Override
                        public void onNext(final List<NotificationDTO> notifications) {
                            callback.clear();
                            for (NotificationDTO notification : notifications)
                                callback.add(notification);
                        }

                        @Override
                        public void onCompleted() {
                            GET_NOTIFICATIONS_CONNECTION_OPEN = false;
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    public static void removeNotification(Activity activity, Notification notification) {
        service.viewNotification(getAuthHeaderFromSharedPreferences(activity), notification.getId())
                .timeout(8, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<String>>() {

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(final List<String> tasks) {
                    }

                    @Override
                    public void onCompleted() {
                    }
                });
    }

    public static void addComment(final CommentsFragment commentFragment, String taskId, String text) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(commentFragment.getActivity());
        String username = prefs.getString("USERNAME", null);
        String userId = prefs.getString("USER_ID", null);

        CommentDTO commentDTO = new CommentDTO(taskId, userId, username, text);
        service.addComment(getAuthHeaderFromSharedPreferences(commentFragment.getActivity()), commentDTO)
                .timeout(4, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<TaskComment>>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        commentFragment.showError("Error while connecting to the server..");
                    }

                    @Override
                    public void onNext(List<TaskComment> response) {
                        commentFragment.clear();
                        for (TaskComment taskComment : response)
                            commentFragment.addToAdapter(taskComment);
                    }
                });
    }


    /**
     * Build the authentification header from Shared Preferences.
     */
    private static String getAuthHeaderFromSharedPreferences(Activity activity) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        String username = prefs.getString("USERNAME", null);
        String password = prefs.getString("PASSWORD", null);

        String base = username + ":" + password;
        String authHeader = "Basic " + Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);
        Log.d("authHeader:", authHeader);
        return authHeader;
    }

}
