package com.endava.bob.bob.view.adapter;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Notification;
import com.endava.bob.bob.domain.constants.NotificationTypeEnum;
import com.endava.bob.bob.service.util.Converter;
import com.endava.bob.bob.view.activity.MainActivity;
import com.endava.bob.bob.view.fragment.NotificationFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Adapter used for displaying notifications.
 * <p>
 * Created by Raul on 3/14/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec
    private final NotificationFragment notificationFragment;

    private Handler handler = new Handler(); // hanlder for running delayed runnables

    private List<Notification> itemsPendingRemoval;
    private HashMap<Notification, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be

    private final List<Notification> notifications;

    public NotificationAdapter(List<Notification> items, NotificationFragment notificationFragment) {
        notifications = items;
        itemsPendingRemoval = new ArrayList<>();
        this.notificationFragment = notificationFragment;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("RB:", "Sunt in onCreateViewHolder()");
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_row, parent, false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, int position) {
        holder.mItem = notifications.get(position);
        String date;
        if (holder.mItem.getCreationDate() != null) {
            date = new Converter().convertWithHour(notifications.get(position).getCreationDate());
        } else {
            date = new Converter().convertWithHour(new Date());
        }

        if (itemsPendingRemoval.contains(notifications.get(position))) {
            // o aratam in "undo" state
            holder.itemView.setBackgroundColor(Color.parseColor("#ff4081"));
            holder.mDateView.setVisibility(View.GONE);
            holder.mContentDetailsView.setVisibility(View.GONE);
            holder.mUndoButton.setVisibility(View.VISIBLE);
            holder.mUndoButton.setVisibility(View.VISIBLE);
            holder.mUndoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = pendingRunnables.get(holder.mItem);
                    pendingRunnables.remove(holder.mItem);
                    if (pendingRemovalRunnable != null)
                        handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(holder.mItem);
                    // this will rebind the row in "normal" state
                    notifyItemChanged(notifications.indexOf(holder.mItem));
                }
            });

        } else {
            //Daca Notificarea nu-i in proces de stergere, o aratam normal
            holder.itemView.setBackgroundColor(Color.WHITE);
            holder.mDateView.setText(date);
            holder.mDateView.setVisibility(View.VISIBLE);
            holder.mContentDetailsView.setText(notifications.get(position).getMessage());
            holder.mContentDetailsView.setVisibility(View.VISIBLE);
            holder.mUndoButton.setVisibility(View.GONE);
            holder.mUndoButton.setOnClickListener(null);
            setBackroundResource(holder.mImageView, holder.mItem.getType());
        }
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void clear() {
        notifications.clear();
        notifyDataSetChanged();
    }

    public void addNotification(Notification task) {
        notifications.add(task);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        Notification item = notifications.get(position);
        if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(item);
        }
        if (notifications.contains(item)) {
            notifications.remove(position);
        }
        notifyItemRemoved(position);
        notificationFragment.removeNotification(item);
    }

    public boolean isUndoOn() {
        return MainActivity.undoValue;
    }

    public boolean isPendingRemoval(int position) {
        Notification item = notifications.get(position);
        return itemsPendingRemoval.contains(item);
    }

    public void pendingRemoval(int position) {
        final Notification item = notifications.get(position);
        if (!itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.add(item);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(notifications.indexOf(item));
                }
            };
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(item, pendingRemovalRunnable);
        }
    }

    /**
     * Set the image of the notification according to the type.
     */
    private void setBackroundResource(ImageView imageView, NotificationTypeEnum type) {
        switch (type) {
            case DME_UPLOADED:
                imageView.setBackgroundResource(R.drawable.fontawesomeenvelope);
                break;
            case DOCUMENT_UPLOADED:
                imageView.setBackgroundResource(R.drawable.fontawesomeupload);
                break;
            case DOSSIER_CREATED:
                imageView.setBackgroundResource(R.drawable.fontawesomeuniversity);
                break;
            case DOSSIER_USER_CHANGE:
                imageView.setBackgroundResource(R.drawable.fontawesomeuniversity);
                break;
            case TASK_CREATED:
                imageView.setBackgroundResource(R.drawable.fontawesometasks);
                break;
            case EDIT_TASK:
                imageView.setBackgroundResource(R.drawable.fontawesometasks);
                break;
            case NEW_COMMENT:
                imageView.setBackgroundResource(R.drawable.fontawesomecomment);
                break;
            case NEW_ACTION:
                imageView.setBackgroundResource(R.drawable.fontawesomepaperplane);
                break;
            case NEW_PERMISSION:
                imageView.setBackgroundResource(R.drawable.fontawesomekey);
                break;
            case NEW_AGREEMENT:
                imageView.setBackgroundResource(R.drawable.fontawesomepaperclip);
                break;
            default:
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mIdView;
        public final TextView mDateView;
        public final TextView mContentDetailsView;
        public final ImageView mImageView;
        public Button mUndoButton;

        public Notification mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.summary);
            mDateView = (TextView) view.findViewById(R.id.content_date);
            mContentDetailsView = (TextView) view.findViewById(R.id.content);
            mUndoButton = (Button) view.findViewById(R.id.undo_button);
            mImageView = (ImageView) view.findViewById(R.id.notif_image_view);
        }

    }

}