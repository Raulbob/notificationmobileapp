package com.endava.bob.bob.service.callback;

import com.endava.bob.bob.domain.domain.dto.UserDTO;

/**
 * Created by Raul on 3/17/2017.
 */

public interface MyAuthCallback {

    void authSucceeded(UserDTO role);

    void authFailed();

    void showError(String error);

}
