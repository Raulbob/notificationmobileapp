package com.endava.bob.bob.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Just24Task implements Serializable {

    private String id;

    private String taskSummary;
    private Date taskCreationDate;
    private Date taskDeadLineDate;
    private String taskPriority;
    private String dossierId;
    private String userIdAssignTo;
    private String userIdAssignFrom;
    private String taskStatus;
    private String taskDetails;
    private ArrayList<TaskComment> comments;

    public Just24Task() {
        super();
    }

    public Just24Task(String id, String taskSummary, Date taskCreationDate, Date taskDeadLineDate, String taskPriority,
                      String dossierId, String userIdAssignTo, String userIdAssignFrom, String taskStatus, String taskDetails) {
        super();
        this.id = id;
        this.taskSummary = taskSummary;
        this.taskCreationDate = taskCreationDate;
        this.taskDeadLineDate = taskDeadLineDate;
        this.taskPriority = taskPriority;
        this.dossierId = dossierId;
        this.userIdAssignTo = userIdAssignTo;
        this.userIdAssignFrom = userIdAssignFrom;
        this.taskStatus = taskStatus;
        this.taskDetails = taskDetails;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskSummary() {
        return taskSummary;
    }

    public void setTaskSummary(String taskSummary) {
        this.taskSummary = taskSummary;
    }

    public Date getTaskCreationDate() {
        return taskCreationDate;
    }

    public void setTaskCreationDate(Date taskCreationDate) {
        this.taskCreationDate = taskCreationDate;
    }

    public Date getTaskDeadLineDate() {
        return taskDeadLineDate;
    }

    public void setTaskDeadLineDate(Date taskDeadLineDate) {
        this.taskDeadLineDate = taskDeadLineDate;
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getDossierId() {
        return dossierId;
    }

    public void setDossierId(String dossierId) {
        this.dossierId = dossierId;
    }

    public String getUserIdAssignTo() {
        return userIdAssignTo;
    }

    public void setUserIdAssignTo(String userIdAssignTo) {
        this.userIdAssignTo = userIdAssignTo;
    }

    public String getUserIdAssignFrom() {
        return userIdAssignFrom;
    }

    public void setUserIdAssignFrom(String userIdAssignFrom) {
        this.userIdAssignFrom = userIdAssignFrom;
    }

    public String getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(String taskDetails) {
        this.taskDetails = taskDetails;
    }

    public String getDaysLeft() {
        Date currentDate = new Date();

        if (taskDeadLineDate != null) {
            if (taskDeadLineDate.after(new Date())) {
                return String.valueOf(TimeUnit.DAYS.convert(this.taskDeadLineDate.getTime() - currentDate.getTime(),
                        TimeUnit.MILLISECONDS));
            } else {
                return "0";
            }
        } else
            return "-";
    }

    public void setComments(ArrayList<TaskComment> comments) {
        this.comments = comments;
    }

    public ArrayList<TaskComment> getComments() {
        return this.comments;
    }
}
