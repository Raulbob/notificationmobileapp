package com.endava.bob.bob.view.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.domain.TaskComment;
import com.endava.bob.bob.view.fragment.CommentsFragment;
import com.endava.bob.bob.view.fragment.TaskDetailsFragment;

import java.util.ArrayList;

/**
 * PageAdapter used in TaskDetailsActivity for interchanging {@link TaskDetailsFragment} with {@link CommentsFragment}.
 * <p>
 * Created by Raul Bob on 6/28/2017.
 */
public class PagerAdapter extends FragmentPagerAdapter {

    private Just24Task task;
    private ArrayList<TaskComment> taskComments;

    public PagerAdapter(FragmentManager fm, Just24Task task, ArrayList<TaskComment> taskComments) {
        super(fm);
        this.task = task;
        this.taskComments = taskComments;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("TASK", task);
        bundle.putSerializable("COMMENTS", taskComments);

        switch (position) {
            case 0:
                TaskDetailsFragment tdf = new TaskDetailsFragment();
                tdf.setArguments(bundle);
                return tdf;
            case 1:
                CommentsFragment cf = new CommentsFragment();
                cf.setArguments(bundle);
                return cf;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Details";
            case 1:
                return "Comments";
            default:
                return null;
        }
    }

}
