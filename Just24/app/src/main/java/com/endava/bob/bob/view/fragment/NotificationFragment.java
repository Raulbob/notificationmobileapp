package com.endava.bob.bob.view.fragment;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Notification;
import com.endava.bob.bob.domain.domain.dto.NotificationDTO;
import com.endava.bob.bob.service.callback.MyNotifCallback;
import com.endava.bob.bob.service.restClient.Manager;
import com.endava.bob.bob.service.util.Converter;
import com.endava.bob.bob.service.util.SwipeHelper;
import com.endava.bob.bob.view.adapter.NotificationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment that displays notifications in a ListView.
 */
public class NotificationFragment extends Fragment implements MyNotifCallback {

    ProgressBar progressBar;
    @BindView(R.id.notification_list)
    RecyclerView recyclerView;

    private Snackbar notificationLoadingError;

    private NotificationAdapter adapter;
    private List<Notification> notifications = new ArrayList<>();

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment, container, false);
        ButterKnife.bind(this, view);

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress);
        Manager.loadNotifications(getActivity(), this, progressBar);
        adapter = new NotificationAdapter(notifications, this);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setHasFixedSize(true);

        SwipeHelper swipeHelper = new SwipeHelper();
        swipeHelper.setUpItemTouchHelper(this.getContext(), recyclerView);
        swipeHelper.setUpAnimationDecoratorHelper(recyclerView);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity myActivity = (AppCompatActivity) getActivity();
        myActivity.getSupportActionBar().setTitle("Notifications");
        //Refresh button
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.refresh_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Manager.loadNotifications(getActivity(), NotificationFragment.this, progressBar);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (notificationLoadingError != null)
            notificationLoadingError.dismiss();
    }

    @Override
    public void add(NotificationDTO entity) {
        Converter c = new Converter();
        adapter.addNotification(c.convertWithHour(entity));
    }

    @Override
    public void clear() {
        adapter.clear();
    }

    @Override
    public void showError(String error) {
        if (!isDetached() && isVisible()) {

            progressBar.setVisibility(View.GONE);

            notificationLoadingError.setText(error);
            notificationLoadingError.show();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        notificationLoadingError = Snackbar.make(recyclerView, "", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Manager.loadNotifications(getActivity(), NotificationFragment.this, progressBar);
                    }
                });
    }

    public void removeNotification(Notification notification) {
        Manager.removeNotification(getActivity(), notification);
    }


}
