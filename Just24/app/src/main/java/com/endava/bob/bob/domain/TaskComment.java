package com.endava.bob.bob.domain;

import java.io.Serializable;

/**
 * POJO for Task comments.
 *
 * @author Raul Bob
 */
public class TaskComment implements Serializable {

    private String id;
    private String taskId;
    private String userId;
    private String userName;
    private String date;
    private String message;

    public TaskComment(String taskId, String userId, String userName, String date, String message) {
        this.taskId = taskId;
        this.userId = userId;
        this.setUserName(userName);
        this.date = date;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "TaskComment{" +
                "id='" + id + '\'' +
                ", taskId='" + taskId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", date='" + date + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

}
