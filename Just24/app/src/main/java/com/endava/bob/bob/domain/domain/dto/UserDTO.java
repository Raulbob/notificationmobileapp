package com.endava.bob.bob.domain.domain.dto;

public class UserDTO {

    private String id;
    private String username;
    private String password;
    private String role;
    private boolean authenticated;

    public UserDTO() {
    }

    public UserDTO(boolean authenticated, String id, String username, String password, String role) {
        this.setAuthenticated(authenticated);
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

}
