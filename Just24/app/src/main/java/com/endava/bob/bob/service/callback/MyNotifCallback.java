package com.endava.bob.bob.service.callback;

import com.endava.bob.bob.domain.domain.dto.NotificationDTO;

/**
 * Created by Raul on 3/14/2017.
 */
public interface MyNotifCallback {
    void add(NotificationDTO entity);

    void clear();

    void showError(String e);
}
