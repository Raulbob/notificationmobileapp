package com.endava.bob.bob.service.callback;

import com.endava.bob.bob.domain.domain.dto.TaskDTO;

/**
 * Created by Raul on 3/13/2017.
 */
public interface MyTaskCallback {
    void add(TaskDTO entity);

    void clear();

    void showError(String e);
}
