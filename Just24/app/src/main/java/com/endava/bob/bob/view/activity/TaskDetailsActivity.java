package com.endava.bob.bob.view.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.domain.TaskComment;
import com.endava.bob.bob.view.adapter.PagerAdapter;
import com.endava.bob.bob.view.fragment.TaskDetailsFragment;

import java.util.ArrayList;

public class TaskDetailsActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private TaskDetailsFragment.OnFragmentInteractionListener mListener;
    private Just24Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        task = (Just24Task) getIntent().getExtras().getSerializable("TASK");
        ArrayList<TaskComment> comments = (ArrayList<TaskComment>) getIntent().getExtras().getSerializable("COMMENTS");

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), task, comments);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setTitle("Details");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Button button = (Button) findViewById(R.id.task_deadline);
        int actualMonth = month + 1;
        button.setText(dayOfMonth + "/" + actualMonth + "/" + year);
    }

    /**
     * When an error occurs, we just finish the Activity.
     */
    public void showError(String e) {
        this.finish();
    }


}
