package com.endava.bob.bob.service.util;

import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.domain.Notification;
import com.endava.bob.bob.domain.domain.dto.NotificationDTO;
import com.endava.bob.bob.domain.domain.dto.TaskDTO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Task used for converting DTO to POJO.
 * <p>
 * Created by Raul on 3/15/2017.
 */
public class Converter {

    /**
     * Convert {@link TaskDTO} to (@link Just24Task}.
     */
    public Just24Task convertWithHour(TaskDTO taskDTO) {
        Just24Task just24Task = new Just24Task();

        just24Task.setId(taskDTO.getId());
        just24Task.setDossierId(taskDTO.getDossierId());
        just24Task.setTaskDetails(taskDTO.getDetalis());
        just24Task.setTaskPriority(taskDTO.getPriority());
        just24Task.setTaskStatus(taskDTO.getStatus());
        just24Task.setTaskSummary(taskDTO.getSummary());
        just24Task.setUserIdAssignFrom(taskDTO.getUserIdAssignFrom());
        just24Task.setUserIdAssignTo(taskDTO.getUserIdAssignTo());
        just24Task.setTaskCreationDate(convertWithHour(taskDTO.getCreatioDate()));
        just24Task.setTaskDeadLineDate(convertWithHour(taskDTO.getDeadline()));
        just24Task.setComments((ArrayList) taskDTO.getComments());

        return just24Task;
    }

    /**
     * Convert {@link NotificationDTO to {@link Notification }}.
     */
    public Notification convertWithHour(NotificationDTO entity) {
        Notification notification = new Notification();

        notification.setCreationDate(convertWithHour(entity.getCreationDate()));
        notification.setId(entity.getId());
        notification.setMessage(entity.getMessage());
        notification.setStatus(entity.getStatus());
        notification.setType(entity.getType());
        notification.setViewDate(entity.getViewDate());
        notification.setUserId(entity.getUserId());

        return notification;
    }

    /**
     * Convert JSON stringDate to {@link Date}.
     */
    public Date convertWithHour(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        return new Date(Long.parseLong(stringDate));
    }

    /**
     * Convert a {@link Date} to displaying String.
     */
    public String convertWithHour(Date date) {
        if (date == null) {
            return null;
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        return df.format(date);
    }

    /**
     * Convert a {@link Date} to displaying String.
     */
    public String convertWithOnlyHour(Date date) {
        if (date == null) {
            return null;
        }
        DateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(date);
    }

    /**
     * Convert a {@link Date} to displaying String.
     */
    public String convert(Date date) {
        if (date == null) {
            return null;
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(date);
    }

}
