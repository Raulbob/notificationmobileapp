package com.endava.bob.bob.domain.constants;

/**
 * Enum used for holding notification statuses.
 *
 * @author Raul Bob
 */
public enum NotificationStatus {
    NEW, VIEWED
}
