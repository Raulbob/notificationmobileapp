package com.endava.bob.bob.view.adapter;


import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.view.fragment.TaskFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * Adapter used for displaying Tasks.
 * <p>
 * Created by Raul on 3/11/2017.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private final List<Just24Task> tasks;
    private final OnListFragmentInteractionListener mListener;

    public TaskAdapter(List<Just24Task> items, OnListFragmentInteractionListener listener) {
        tasks = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = tasks.get(position);
        int i = position + 1;
        holder.mSummary.setText(tasks.get(position).getTaskSummary());
        String status = tasks.get(position).getTaskStatus();
        holder.mStatus.setText(status);

        differentiateRowsByStatus(holder, status);

        String descriptionFragm = tasks.get(position).getTaskDetails();
        if (descriptionFragm != null)
            if (descriptionFragm.length() > 35) {
                descriptionFragm = descriptionFragm.substring(0, 35) + "..";
            }
        holder.mDescription.setText(descriptionFragm);
        holder.itemView.setBackgroundColor(Color.WHITE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    private void differentiateRowsByStatus(ViewHolder holder, String status) {
        int textColor;
        if (status != null)
            switch (status) {
                case "New":
                    textColor = Color.parseColor("#ff4081");
                    holder.mStatus.setTextColor(textColor);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.mRadioButton.setButtonTintList(ColorStateList.valueOf(textColor));
                    }
                    holder.mRadioButton.setVisibility(View.VISIBLE);
                    break;
                case "Work":
                    textColor = Color.parseColor("#929c0909");
                    holder.mStatus.setTextColor(textColor);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.mRadioButton.setButtonTintList(ColorStateList.valueOf(textColor));
                    }
                    holder.mRadioButton.setVisibility(View.VISIBLE);
                    break;
                case "Review":
                    textColor = Color.parseColor("#929c0909");
                    holder.mStatus.setTextColor(textColor);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.mRadioButton.setButtonTintList(ColorStateList.valueOf(textColor));
                    }
                    holder.mRadioButton.setVisibility(View.VISIBLE);
                    break;
                case "Done":
                    textColor = Color.parseColor("#7bc0ce");
                    holder.mStatus.setTextColor(textColor);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.mRadioButton.setButtonTintList(ColorStateList.valueOf(textColor));
                    }
                    holder.mRadioButton.setVisibility(View.GONE);
                    break;
            }
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void clear() {
        tasks.clear();
        notifyDataSetChanged();
    }

    public void addTask(Just24Task task) {
        tasks.add(task);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mSummary;
        public final TextView mStatus;
        public final TextView mDescription;
        public final RadioButton mRadioButton;

        public Just24Task mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mSummary = (TextView) view.findViewById(R.id.summary);
            mStatus = (TextView) view.findViewById(R.id.content);
            mDescription = (TextView) view.findViewById(R.id.contentDetails);
            mRadioButton = (RadioButton) view.findViewById(R.id.taskRadioButton);
        }

    }
}
