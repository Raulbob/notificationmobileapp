package com.endava.bob.bob.view.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.domain.TaskComment;
import com.endava.bob.bob.service.restClient.Manager;
import com.endava.bob.bob.view.activity.TaskDetailsActivity;
import com.endava.bob.bob.view.adapter.CommentsAdapter;
import com.endava.bob.bob.view.decorator.DividerItemDecoration;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;

/**
 * Fragment used for displaying comments.
 */
public class CommentsFragment extends Fragment {

    private RecyclerView recyclerView;
    private CommentsAdapter adapter;

    private ArrayList<TaskComment> comments;
    private Just24Task task;

    public CommentsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);
        Bundle bundle = getArguments();
        comments = (ArrayList<TaskComment>) bundle.getSerializable("COMMENTS");
        task = (Just24Task) bundle.getSerializable("TASK");

        recyclerView = (RecyclerView) view.findViewById(R.id.comments_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String username = prefs.getString("USERNAME", null);
        adapter = new CommentsAdapter(comments, username);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity().getApplicationContext(),
                R.drawable.item_decorator)));

        recyclerView.setAdapter(adapter);
        recyclerView.smoothScrollToPosition(comments.size());

        final EditText editText = (EditText) view.findViewById(R.id.comment_edit_text);
        ImageView sendButton = (ImageView) view.findViewById(R.id.send_comment);
        sendButton.setClickable(true);
        sendButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (!editText.getText().toString().trim().equals("")) {
                    addComment(editText.getText().toString());
                    editText.getText().clear();
                }
                return true;
            }
        });

        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (isOpen)
                            recyclerView.smoothScrollToPosition(adapter.getItemCount());
                    }
                });

        return view;
    }

    /**
     * Makes a new Comment Request to the Server. If the server returns "true", the comment is also added to RecycleView.
     */
    private void addComment(String text) {
        Manager.addComment(this, task.getId(), text);
    }

    /**
     * Show error in case of Network Exception.
     */
    public void showError(String s) {
        ((TaskDetailsActivity) getActivity()).showError(s);
    }

    /**
     * Remove all elements from the list of comments.
     */
    public void clear() {
        adapter.clear();
    }

    /**
     * Add a comment to the list.
     */
    public void addToAdapter(TaskComment taskComment) {
        adapter.addComment(taskComment);
        recyclerView.smoothScrollToPosition(adapter.getItemCount());
    }
}
