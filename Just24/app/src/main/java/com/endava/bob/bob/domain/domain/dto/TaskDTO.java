package com.endava.bob.bob.domain.domain.dto;

import com.endava.bob.bob.domain.TaskComment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raul on 3/15/2017.
 */
public class TaskDTO {

    private String id;

    private String summary;
    private String creationDate;
    private String deadline;
    private String priority;
    private String dossierId;
    private String userIdAssignTo;
    private String userIdAssignFrom;
    private String status;
    private String details;
    private List<TaskComment> comments;

    public TaskDTO() {
    }

    public TaskDTO(String id, String taskSummary, String taskCreationDate, String taskDeadLineDate, String taskPriority,
                   String dossierId, String userIdAssignTo, String userIdAssignFrom, String taskStatus, String taskDetails) {
        this.id = id;
        this.summary = taskSummary;
        this.creationDate = taskCreationDate;
        this.deadline = taskDeadLineDate;
        this.priority = taskPriority;
        this.dossierId = dossierId;
        this.userIdAssignTo = userIdAssignTo;
        this.userIdAssignFrom = userIdAssignFrom;
        this.status = taskStatus;
        this.details = taskDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String taskStatus) {
        this.status = taskStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String taskSummary) {
        this.summary = taskSummary;
    }

    public String getCreatioDate() {
        return creationDate;
    }

    public void setCreationDate(String taskCreationDate) {
        this.creationDate = taskCreationDate;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String taskDeadLineDate) {
        this.deadline = taskDeadLineDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String taskPriority) {
        this.priority = taskPriority;
    }

    public String getDossierId() {
        return dossierId;
    }

    public void setDossierId(String dossierId) {
        this.dossierId = dossierId;
    }

    public String getUserIdAssignTo() {
        return userIdAssignTo;
    }

    public void setUserIdAssignTo(String userIdAssignTo) {
        this.userIdAssignTo = userIdAssignTo;
    }

    public String getUserIdAssignFrom() {
        return userIdAssignFrom;
    }

    public void setUserIdAssignFrom(String userIdAssignFrom) {
        this.userIdAssignFrom = userIdAssignFrom;
    }

    public String getDetalis() {
        return details;
    }

    public void setDetails(String taskDetails) {
        this.details = taskDetails;
    }

    public List<TaskComment> getComments() {
        if (comments == null) {
            comments = new ArrayList<>();
        }
        return comments;
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
                "id='" + id + '\'' +
                ", summary='" + summary + '\'' +
                ", creationDate=" + creationDate +
                ", deadline=" + deadline +
                ", priority='" + priority + '\'' +
                ", dossierId='" + dossierId + '\'' +
                ", userIdAssignTo='" + userIdAssignTo + '\'' +
                ", userIdAssignFrom='" + userIdAssignFrom + '\'' +
                ", status='" + status + '\'' +
                ", details='" + details + '\'' +
                ", comments=" + comments +
                '}';
    }

}