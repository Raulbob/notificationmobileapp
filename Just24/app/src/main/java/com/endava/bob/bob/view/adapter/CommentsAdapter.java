package com.endava.bob.bob.view.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.TaskComment;
import com.endava.bob.bob.service.util.Converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Adaptor for Comments RecycleView. The List has 3 types of cells. The comment of the current user, different user and the date at which the following comments have been added.
 */
public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_DATE = 0;
    private final int VIEW_TYPE_STRANGER_USER = 1;
    private final int VIEW_TYPE_CURRENT_USER = 2;

    private final List<TaskComment> comments;
    private final Map<String, List<TaskComment>> commentsByDate;
    private final String currentUser;
    private List<Integer> datePositions;

    public CommentsAdapter(List<TaskComment> items, String currentUser) {
        comments = items;
        this.currentUser = currentUser;
        commentsByDate = new LinkedHashMap<>();
        populateCommentsMap(commentsByDate);
        datePositions = new ArrayList<>();
        populateDatePositions(datePositions);
    }

    private void populateDatePositions(List<Integer> datePosition) {
        int pos = -1;
        for (String key : commentsByDate.keySet()) {
            pos++;
            datePosition.add(pos);
            comments.add(pos, null);
            pos += commentsByDate.get(key).size();
        }
    }

    private void populateCommentsMap(Map<String, List<TaskComment>> commentsByDate) {
        for (TaskComment taskComment : comments) {
            Converter converter = new Converter();
            String date = converter.convert(converter.convertWithHour(taskComment.getDate()));
            if (commentsByDate.containsKey(date)) {
                commentsByDate.get(date).add(taskComment);
            } else {
                commentsByDate.put(date, new ArrayList<TaskComment>());
                commentsByDate.get(date).add(taskComment);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (datePositions.contains(position)) {
            return VIEW_TYPE_DATE;
        } else {
            TaskComment taskComment = comments.get(position);
            if (!taskComment.getUserName().equals(currentUser)) {
                return VIEW_TYPE_STRANGER_USER;
            } else {
                return VIEW_TYPE_CURRENT_USER;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case VIEW_TYPE_DATE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_date_row, parent, false);
                return new DateViewHolder(view);

            case VIEW_TYPE_STRANGER_USER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.comment_row, parent, false);
                break;
            case VIEW_TYPE_CURRENT_USER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.comment_row2, parent, false);
                break;
        }

        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        int posInDatePositions = datePositions.lastIndexOf(position);
        //if it is a DATE row
        if (posInDatePositions != -1) {
            DateViewHolder viewHolder = (DateViewHolder) holder;
            int i = 0;
            for (String key : commentsByDate.keySet()) {
                if (i == posInDatePositions) {
                    viewHolder.date.setText(key);
                    return;
                }
                i++;
            }
        } else {
            //if it's a COMMENT_ROW
            CommentViewHolder viewHolder = (CommentViewHolder) holder;
            viewHolder.mItem = comments.get(position);
            int i = position + 1;
            viewHolder.text.setText(viewHolder.mItem.getMessage());
            viewHolder.author.setText(viewHolder.mItem.getUserName());
            if (viewHolder.mItem.getUserName().equals(currentUser)) {
                viewHolder.author.setTextColor(Color.DKGRAY);
            }
            Date date = new Converter().convertWithHour(viewHolder.mItem.getDate());
            viewHolder.date.setText(new Converter().convertWithOnlyHour(date));
            holder.itemView.setBackgroundColor(Color.WHITE);
        }

    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void clear() {
        comments.clear();
        notifyDataSetChanged();
    }

    public void addComment(TaskComment comment) {
        comments.add(comment);
        notifyDataSetChanged();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView text;
        public final TextView author;
        public final TextView date;

        public TaskComment mItem;

        public CommentViewHolder(View view) {
            super(view);
            mView = view;
            text = (TextView) view.findViewById(R.id.comment_text);
            author = (TextView) view.findViewById(R.id.comment_author);
            date = (TextView) view.findViewById(R.id.comment_date);
        }

    }

    public class DateViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView date;

        public DateViewHolder(View view) {
            super(view);
            mView = view;
            date = (TextView) view.findViewById(R.id.comment_date);
        }

    }

}
