package com.endava.bob.bob.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.view.fragment.NotificationFragment;
import com.endava.bob.bob.view.fragment.TaskFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Main Activity on which the fragments will be attached.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TaskFragment.OnListFragmentInteractionListener {

    private NotificationFragment notificationFragment = new NotificationFragment();
    private TaskFragment taskFragment = new TaskFragment();

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
//    @BindView(R.id.current_user_nav_drawer_text_view)
//    TextView currentUserTextView;

    public static boolean undoValue = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bob app");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View view = navigationView.getHeaderView(0);
        TextView textView = (TextView) view.findViewById(R.id.current_user_nav_drawer_text_view);
        textView.setText(getIntent().getStringExtra("USERNAME"));


        attachMainFragment();
    }

    private void attachMainFragment() {
        TaskFragment fragment = new TaskFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.relativelayout_for_fragment, fragment).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        FragmentManager manager = getSupportFragmentManager();

        int id = item.getItemId();

        if (id == R.id.nav_tasks) {
            // Handle the Tasks action
            manager.beginTransaction().replace(R.id.relativelayout_for_fragment, taskFragment).commit();

        } else if (id == R.id.nav_notifications) {
            manager.beginTransaction().replace(R.id.relativelayout_for_fragment, notificationFragment).commit();


        } else if (id == R.id.nav_logout) {
            //if logout, clean the sharedPreferences
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            prefs.edit().putBoolean("IS_LOGIN", false).commit();
            prefs.edit().putString("USERNAME", null).commit();
            prefs.edit().putString("ROLE", null).commit();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(Just24Task item) {
        Intent intent = new Intent(getApplicationContext(), TaskDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("TASK", item);
        bundle.putSerializable("COMMENTS", item.getComments());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_undo_checkbox) {
            item.setChecked(!item.isChecked());
            this.undoValue = item.isChecked();
        }

        return super.onOptionsItemSelected(item);
    }

}
