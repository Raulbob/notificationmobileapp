package com.endava.bob.bob.view.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.endava.bob.bob.R;
import com.endava.bob.bob.domain.Just24Task;
import com.endava.bob.bob.service.util.Converter;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment that displays a certain Task details.
 */
public class TaskDetailsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Just24Task task;

    @BindView(R.id.task_sumarry)
    TextView summaryTextView;

    @BindView(R.id.task_priority)
    TextView priorityTextView;

    @BindView(R.id.task_dossier)
    TextView dossierTextView;

    @BindView(R.id.task_user_responsable)
    TextView lawyerTextView;

    @BindView(R.id.task_status)
    TextView statusTextView;

    @BindView(R.id.task_details)
    TextView detailsTextView;

    @BindView(R.id.task_deadline)
    Button deadlineButton;

    public TaskDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle = getArguments();

        task = (Just24Task) bundle.getSerializable("TASK");
        View view = inflater.inflate(R.layout.fragment_task_details, container, false);
        ButterKnife.bind(this, view);

        summaryTextView.setText(task.getTaskSummary());
        priorityTextView.setText(task.getTaskPriority());
        dossierTextView.setText(task.getDossierId());
        lawyerTextView.setText(task.getUserIdAssignTo());
        statusTextView.setText(task.getTaskStatus());
        detailsTextView.setText(task.getTaskDetails());
        deadlineButton.setText(new Converter().convert(task.getTaskDeadLineDate()));
        deadlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildDatePicker(v);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity myActivity = (AppCompatActivity) getActivity();
        myActivity.getSupportActionBar().setTitle("Tasks");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void buildDatePicker(View view) {
        DatePickerFragment fr = new DatePickerFragment();
        fr.show(getActivity().getSupportFragmentManager(), "date");
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public static class DatePickerFragment extends DialogFragment {

        public DatePickerFragment() {
            // Required empty public constructor
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        }

    }

}
