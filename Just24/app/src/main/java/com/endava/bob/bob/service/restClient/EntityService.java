package com.endava.bob.bob.service.restClient;

import com.endava.bob.bob.domain.TaskComment;
import com.endava.bob.bob.domain.domain.dto.CommentDTO;
import com.endava.bob.bob.domain.domain.dto.NotificationDTO;
import com.endava.bob.bob.domain.domain.dto.TaskDTO;
import com.endava.bob.bob.domain.domain.dto.UserDTO;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Raul on 3/13/2017.
 */
public interface EntityService {

    String SERVICE_ENDPOINT = "http://91.209.189.38:8080/prototypeJust24_Prototype3_Upgrade_05Sep2017/mobile/";

    @POST("authenticate")
    Observable<UserDTO> authenticate(@Header("Authorization") String authHeader, @Body UserDTO userDTO);

    @GET("tasks")
    Observable<List<TaskDTO>> getTasks(@Header("Authorization") String authHeader, @Query("username") String username);

    @GET("notifications")
    Observable<List<NotificationDTO>> getNotifications(@Header("Authorization") String authHeader, @Query("username") String username);

    @GET("viewNotification")
    Observable<List<String>> viewNotification(@Header("Authorization") String authHeader, @Query("notificationId") String notificationId);

    @POST("addComment")
    Observable<List<TaskComment>> addComment(@Header("Authorization") String authHeader, @Body CommentDTO commentDTO);

}
