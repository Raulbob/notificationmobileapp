package com.endava.bob.bob.domain.domain.dto;

/**
 * DTO Object for Comments.
 */
public class CommentDTO {

    private String taskId;
    private String userId;
    private String userName;
    private String text;

    public CommentDTO(String taskId, String userId, String userName, String text) {
        this.taskId = taskId;
        this.userId = userId;
        this.userName = userName;
        this.text = text;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
